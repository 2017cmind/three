﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Areas.Admin.ViewModels.Banner;
using Three.Areas.Admin.ViewModels.News;
using Three.Areas.Admin.ViewModels.Project;
using Three.Models;

namespace Three.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台            
                cfg.CreateMap<News, NewsView>();
                cfg.CreateMap<NewsView, News>();

                cfg.CreateMap<Banner, BannerView>();
                cfg.CreateMap<BannerView, Banner>();

                cfg.CreateMap<Project, ProjectView>();
                cfg.CreateMap<ProjectView, Project>();
                cfg.CreateMap<Project, ProjectIndexItemView>();

                #endregion

                #region 前台
                cfg.CreateMap<Project, ViewModels.Project.ProjectView>();
                cfg.CreateMap<ViewModels.Project.ProjectView, Project>();

                cfg.CreateMap<Project, ViewModels.Project.ProjectPhotoView>();
                cfg.CreateMap<ViewModels.Project.ProjectPhotoView, Project>();

                cfg.CreateMap<Banner, ViewModels.Banner.BannerView>();
                cfg.CreateMap<ViewModels.Banner.BannerView, Banner>();

                cfg.CreateMap<ProjectType, ViewModels.Project.ProjectView>();
                cfg.CreateMap<ProjectType, ViewModels.Project.ProjectTypeListView>();
                cfg.CreateMap<Project, ViewModels.Project.ProjectTypeListView>();
                #endregion

            });
        }
    }
}