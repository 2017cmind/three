﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models.Cmind;

namespace Three.ViewModels.News
{
    public class NewsQuery : PageQuery
    {
        public NewsQuery()
        {
            this.Sorting = "OnlineTime";
            this.isDescending = true;
        }
    }
}