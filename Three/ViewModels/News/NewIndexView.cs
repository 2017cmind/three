﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models.Cmind;

namespace Three.ViewModels.News
{
    public class NewsIndexView : NewsQuery
    {
        public PageResult<NewsView> PageResult { get; set; }
    }
}