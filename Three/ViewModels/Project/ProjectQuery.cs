﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models.Cmind;

namespace Three.ViewModels.Project
{
    public class ProjectQuery : PageQuery
    {
        public ProjectQuery()
        {
            this.Sorting = "Sort";
            this.isDescending = false;
        }
    }
}