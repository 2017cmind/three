﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models.Cmind;

namespace Three.ViewModels.Project
{
    public class ProjectTypeListView : ProjectQuery
    {
        public PageResult<ProjectView> PageResult { get; set; }

        public List<ProjectView> ProjectTypeList { get; set; }
        public List<ProjectView> ProjectList { get; set; }

        public string ID { get; set; }

        public string Title { get; set; }

        public string Image { get; set; }

    }
}