﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.ViewModels.Project
{
    public class ProjectIndexView
    {
        public int SearchArea { get; set; }

        public List<SelectListItem> AreaList { get; set; }

        public IEnumerable<ProjectTypeListView> TypeList { get; set; }
        
        public List<ProjectView> ProjectAreaList { get; set; }

        public ProjectView ProjectAreaData { get; set; }
    }
}