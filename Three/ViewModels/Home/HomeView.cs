﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.ViewModels.Banner;
using Three.ViewModels.News;
using Three.ViewModels.Project;

namespace Three.ViewModels.Home
{
    public class HomeView
    {
        public IEnumerable<NewsView> NewsList { get; set; }
        public IEnumerable<BannerView> PCBannerList { get; set; }
        public IEnumerable<BannerView> MoblieBannerList { get; set; }
    }
}