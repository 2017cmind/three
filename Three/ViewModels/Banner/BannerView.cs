﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Three.ViewModels.Banner
{
    public class BannerView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "輪播圖片")]
        public string FileName { get; set; }

        [Display(Name = "圖片類型")]
        public int Type { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "敘述")]
        public string Description { get; set; }

        [Display(Name = "案例類型")]
        public int ProjectID { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }
    }
}