﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Three.Models
{
    public class EnumClass
    {
        public enum BannerType
        {
            /// <summary>
            /// 電腦版
            /// </summary>
            [Description("電腦版")]
            Computer = 1,

            /// <summary>
            /// 手機板       
            /// </summary>
            [Description("手機板")]
            Phone = 2,

        }
    }
}