﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Three.Models.Cmind
{
    public class ImageCheckResult
    {
        public ImageCheckResult()
        {
            HasFile = false;
            ErrorMessage = "Server Error";
        }

        /// <summary>
        /// 是否有檔案
        /// </summary>
        public bool HasFile { get; set; }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}