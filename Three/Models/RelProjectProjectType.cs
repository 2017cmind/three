﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Three.Models
{
    public class RelProjectProjectType
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public bool Status { get; set; }        
        public int Sort { get; set; }

        public int Type { get; set; }
        public string ProjectTypeTitle { get; set; }
        public bool ProjectTypeStatus { get; set; }
        public int ProjectTypeSort { get; set; }

        public int AreaID { get; set; }
        public string AreaTitle { get; set; }
        public bool AreaStatus { get; set; }

        public int ProjectID { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public bool ProjectImageStatus { get; set; }
        public int ProjectImageSort { get; set; }


    }
}