﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.Areas.Admin.ViewModels.Banner
{
    public class BannerIndexView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "輪播圖片")]
        public string FileName { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "輪播圖片類型")]
        public string Type { get; set; }

        [Display(Name = "啟用狀態")]
        public string Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        public IEnumerable<BannerView> DataList { get; set; }

        public List<SelectListItem> BannerTypeOptions { get; set; }

        public List<SelectListItem> StatusOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                result.Add(new SelectListItem { Text = "全部", Value = "" });
                result.Add(new SelectListItem { Text = "啟用", Value = "1" });
                result.Add(new SelectListItem { Text = "停用", Value = "0" });
                return result;
            }
        }

    }
}