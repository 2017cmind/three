﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Three.Areas.Admin.ViewModels.ProjectType
{
    public class ProjectTypeView
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        public int Creater { get; set; }
        public DateTime CreateTime { get; set; }
        public int Updater { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}