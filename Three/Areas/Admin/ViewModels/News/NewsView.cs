﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Three.Areas.Admin.ViewModels.News
{
    public class NewsView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "主圖")]
        public string MainImage { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "副標題")]
        public string SubTitle { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }
    }
}