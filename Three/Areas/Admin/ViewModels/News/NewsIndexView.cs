﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.Models.Cmind;

namespace Three.Areas.Admin.ViewModels.News
{
    public class NewsIndexView
    {
        public int ID { get; set; }

        /// <summary>
        /// 標題
        /// </summary>
        [Display(Name = "標題")]
        public string Title { get; set; }

        /// <summary>
        /// 副標題
        /// </summary>
        [Display(Name = "副標題")]
        public string SubTitle { get; set; }

        /// <summary>
        /// 上線時間
        /// </summary>
        [Display(Name = "上線時間")]
        public string OnlineTime { get; set; }

        /// <summary>
        /// 啟用狀態
        /// </summary>
        [Display(Name = "啟用狀態")]
        public string Status { get; set; }

        public IEnumerable<NewsIndexItemView> DataList { get; set; }

        #region 畫面邏輯

        public List<SelectListItem> StatusOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                result.Add(new SelectListItem { Text = "全部", Value = "" });
                result.Add(new SelectListItem { Text = "啟用", Value = "1" });
                result.Add(new SelectListItem { Text = "停用", Value = "0" });
                return result;
            }
        }
    }
        #endregion
        public class NewsIndexItemView
        {
            public int ID { get; set; }

            public bool Status { get; set; }

            public DateTime UpdateTime { get; set; }

            public string Title { get; set; }

            public string SubTitle { get; set; }

            public DateTime OnlineTime { get; set; }
        }
}