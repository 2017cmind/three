﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.Areas.Admin.ViewModels.Project
{
    public class ProjectIndexView
    {
        public int ID { get; set; }

        /// <summary>
        /// 啟用狀態
        /// </summary>
        [Display(Name = "啟用狀態")]
        public string Status { get; set; }

        /// <summary>
        /// 標題
        /// </summary>
        [Display(Name = "標題")]
        public string Title { get; set; }

        /// <summary>
        /// 類型
        /// </summary>
        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        public IEnumerable<ProjectIndexItemView> DataList { get; set; }

        [Display(Name = "區域")]
        public int AreaID { get; set; }

        public List<SelectListItem> AreaOptions { get; set; }
        
        #region 畫面邏輯

        public List<SelectListItem> StatusOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                result.Add(new SelectListItem { Text = "全部", Value = "" });
                result.Add(new SelectListItem { Text = "啟用", Value = "1" });
                result.Add(new SelectListItem { Text = "停用", Value = "0" });
                return result;
            }
        }

        public List<SelectListItem> TypeOptions { get; set; }
        #endregion
    }

    public class ProjectIndexItemView
    {
        public int ID { get; set; }

        public bool Status { get; set; }

        public System.DateTime UpdateTime { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public int Sort { get; set; }

        public int AreaID { get; set; }
    }
}