﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.Areas.Admin.ViewModels.Project
{
    public class ProjectView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "主圖")]
        public string MainImage { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "副標題")]
        public string SubTitle { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "區域ID")]
        public int AreaID { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "坪數")]
        public string Footage { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        public List<SelectListItem> TypeList { get; set; }

        public List<SelectListItem> AreaList { get; set; }
    }
}