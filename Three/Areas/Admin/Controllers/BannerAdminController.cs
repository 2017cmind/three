﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Areas.Admin.ViewModels.Banner;
using Three.Models;
using Three.Models.Cmind;
using Three.Repositories;
using Three.Utility.Cmind;
using static Three.Models.EnumClass;

namespace Three.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class BannerAdminController : BaseAdminController
    {
        private BannerRepository bannerRepository = new BannerRepository();
        private ProjectRepository projectRepository = new ProjectRepository();

        public ActionResult Index(BannerIndexView model)
        {
            var dataList = bannerRepository.Query(model.Status, model.Title, model.Type, 0);
            model.DataList = Mapper.Map<IEnumerable<BannerView>>(dataList);

            var TypeListOptions = GetTypeListItem();
            TypeListOptions.Insert(0, new SelectListItem { Text = "請選擇", Value = "" });
            model.BannerTypeOptions = TypeListOptions;
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new BannerView();
            if (id != 0)
            {
                var query = bannerRepository.FindBy(id);
                model = Mapper.Map<BannerView>(query);
            }
            model = getDefault(model);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(BannerView model, HttpPostedFileBase file)
        {
            ImageCheckResult checkResult = ImageHelper.ImageCheck(file);

            bool validate = true;
            if (!checkResult.HasFile && model.ID == 0)
            {
                validate = false;
                ShowMessage(false, "請上傳圖片");
            }

            if (ModelState.IsValid && validate)
            {
                Banner data = Mapper.Map<Banner>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                //檔案處裡
                bool hasFile = ImageHelper.CheckFileExists(file);
                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, model.FileName);
                    data.FileName = ImageHelper.SaveImage(file, PhotoFolder);
                }

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    model.ID = bannerRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    bannerRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }
                return RedirectToAction("Edit", new { id = model.ID });
            }

            model = getDefault(model);

            return View(model);
        }

        public ActionResult Delete(int id ,int projectId = 0)
        {          
            string file = bannerRepository.DeleteImage(id);
            ImageHelper.DeletePhoto(PhotoFolder, file);
            bannerRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            if (projectId == 0)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("ProjectImage", new { projectId = projectId });
            }
        }

        public ActionResult ProjectImage(int projectid = 0)
        {
            var model = new BannerView();

            model.ProjectID = projectid;
            var query = projectRepository.FindBy(projectid);
            model.ProjectTitle = query.Title;
            model.ProjectType = query.Type;
            var dataList = bannerRepository.GetImagesById(projectid).ToList();
            model.DataList = Mapper.Map<IEnumerable<BannerView>>(dataList);

            return View(model);
        }

        [HttpPost]
        public ActionResult ProjectImage(BannerView model, HttpPostedFileBase file)
        {
            ImageCheckResult checkResult = ImageHelper.ImageCheck(file);

            bool validate = true;
            if (!checkResult.HasFile && model.ID == 0)
            {
                ModelState.AddModelError("FileName", checkResult.ErrorMessage);
                validate = false;
                ShowMessage(false, "請上傳圖片");
            }

            if (ModelState.IsValid && validate)
            {
                Banner data = Mapper.Map<Banner>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                //檔案處裡
                bool hasFile = ImageHelper.CheckFileExists(file);
                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, model.FileName);
                    data.FileName = ImageHelper.SaveImage(file, PhotoFolder);
                }

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    bannerRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    bannerRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }
            }

            var dataList = bannerRepository.GetImagesById(model.ProjectID);
            model.DataList = Mapper.Map<IEnumerable<BannerView>>(dataList);
            var query = projectRepository.FindBy(model.ProjectID);
            model.ProjectTitle = query.Title;
            model.ProjectType = query.Type;
            return View(model);
        }


        /// <summary>
        /// Banner圖片分類清單
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetTypeListItem()
        {
            var TypeList = Enum.GetValues(typeof(BannerType));
            var result = new List<SelectListItem>();
            foreach (var item in TypeList)
            {
                result.Add(new SelectListItem()
                {
                    Text = EnumHelper.GetDescription((BannerType)item),
                    Value = ((int)item).ToString(),
                });
            }
            return result;
        }
        private BannerView getDefault(BannerView model)
        {
            model.BannerTypeOptions = GetTypeListItem();
            return model;
        }
    }
}