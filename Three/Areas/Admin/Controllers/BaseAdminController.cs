﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.Areas.Admin.Controllers
{
    public class BaseAdminController : Controller
    {
        private readonly string fileUploads = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploads"];

        public string PhotoFolder
        {
            get
            {
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                string photoFolder = controllerName.Replace("Admin", "Photo");
                string savePath = Path.Combine(Server.MapPath(fileUploads), photoFolder);
                return savePath;
            }
        }

        /// <summary>
        /// 顯示訊息
        /// 成功時為綠色訊息
        /// 失敗時為紅色訊息
        /// </summary>        
        /// <param name="success">
        /// 是否成功 成功:true 失敗:false
        /// </param>
        /// <param name="message">
        /// 要顯示的訊息，若帶入空字串則預設成[success==true]為"成功"，[success==false]為"失敗"
        /// </param>
        public void ShowMessage(bool success, string message)
        {
            string tempDataKey = success ? "Result" : "Error";

            if (string.IsNullOrEmpty(message))
                message = success ? "成功" : "失敗";

            this.TempData[tempDataKey] = message;
        }

        /// <summary>
        /// 找不到時的頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult NotFound()
        {
            return View("~/Areas/Admin/Views/Shared/NotFound.cshtml");
        }

        /// <summary>
        /// 錯誤頁面
        /// </summary>
        /// <param name="errorMessage">
        /// 錯誤訊息
        /// </param>
        /// <returns></returns>
        public ActionResult Error(string errorMessage = "")
        {
            TempData["ErrorMessage"] = errorMessage;
            return View("~/Areas/Admin/Views/Shared/Error.cshtml");
        }
    }
}