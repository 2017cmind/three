﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Areas.Admin.ViewModels.Project;
using Three.Models;
using Three.Repositories;
using Three.Utility.Cmind;
using static Three.Models.EnumClass;

namespace Three.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProjectAdminController : BaseAdminController
    {
        private ProjectRepository projectRepository = new ProjectRepository();
        private ProjectTypeRepository projectTypeRepository = new ProjectTypeRepository();
        private readonly string fileUploads = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploads"];

        public ActionResult Index(ProjectIndexView model)
        {
            var query = projectRepository.Query(model.Status, model.Title, model.AreaID, model.Type);

            IEnumerable<ProjectIndexItemView> data = Mapper.Map<IEnumerable<ProjectIndexItemView>>(query);
            model.DataList = data;


            var TypeListOptions = GetTypeListItem();
            TypeListOptions.Insert(0, new SelectListItem { Text = "請選擇", Value = "" });
            model.TypeOptions = TypeListOptions;

            var AreaListOptions = GetAreaListItem();
            AreaListOptions.Insert(0, new SelectListItem { Text = "請選擇", Value = "" });
            model.AreaOptions = AreaListOptions;
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ProjectView();
            if (id != 0)
            {
                model = Mapper.Map<ProjectView>(projectRepository.FindBy(id));
            }

            model.TypeList = GetTypeListItem();
            model.AreaList = GetAreaListItem();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ProjectView model, HttpPostedFileBase file)
        {
            var hasFile = ImageHelper.CheckFileExists(file);

            if (ModelState.IsValid)
            {
                Models.Project project = Mapper.Map<Models.Project>(model);

                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, project.MainImage);
                    project.MainImage = ImageHelper.SaveImage(file, PhotoFolder);
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                project.Updater = adminId;
               
                if (model.ID == 0)
                {
                    project.Creater = adminId;
                    if (model.Type == 2)
                    {
                        project.AreaID = 0;
                    }
                    model.ID = projectRepository.Insert(project);

                    ShowMessage(true, "新增成功");
                }
                else
                {
                    if (model.Type == 2)
                    {
                        project.AreaID = 0;
                    }
                    projectRepository.Update(project);
                    ShowMessage(true, "修改成功");
                }
                return RedirectToAction("Edit", new { id = model.ID });
            }
            model.TypeList = GetTypeListItem();
            model.AreaList = GetAreaListItem();

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            DeleteImage(id);

                #region 刪除banner資料表有該Projectid所有圖片
                var imageList = projectRepository.DeleteImageList(id);
                string savePath = Path.Combine(Server.MapPath(fileUploads), "BannerPhoto");
                foreach (var item in imageList)
                {
                    ImageHelper.DeletePhoto(savePath, item.ToString());
                }
                #endregion

            projectRepository.Delete(id);

            ShowMessage(true, "刪除成功");

            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id)
        {
            string image = projectRepository.DeleteImage(id);
            ImageHelper.DeletePhoto(PhotoFolder, image);
            ShowMessage(true, "刪除圖片成功");
            return RedirectToAction("Edit", new { id = id });
        }

        /// <summary>
        /// 案例分類清單
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetTypeListItem()
        {
            List<ProjectType> TypeList = projectTypeRepository.List().ToList();
            var result = new List<SelectListItem>();
            foreach (var item in TypeList)
            {
                result.Add(new SelectListItem()
                {
                    Text = item.Title,
                    Value = item.ID.ToString(),
                });
            }
            return result;
        }

        /// <summary>
        /// 案例區域清單
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetAreaListItem()
        {
            List<SelectListItem> areaList = new List<SelectListItem>();
            List<Area> AreaList = projectRepository.GetAllAreas().Where(a => a.Status == true).ToList();
            for (int i = 0; i < AreaList.Count; i++)
            {
                areaList.Add(new SelectListItem()
                {
                    Text = AreaList[i].Title,
                    Value = (AreaList[i].ID).ToString(),
                    Selected = false
                });
            }
                
            return areaList;
        }

        /// <summary>
        /// html編輯器圖片上傳
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string result = "";
            if (upload != null && upload.ContentLength > 0)
            {
                //儲存圖片至Server
                upload.SaveAs(Server.MapPath("~/FileUploads/ImagePhoto/" + upload.FileName));


                var imageUrl = Url.Content("~/FileUploads/ImagePhoto/" + upload.FileName);

                var vMessage = string.Empty;

                result = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imageUrl + "\", \"" + vMessage + "\");</script></body></html>";

            }
            return Content(result);
        }
    }
}