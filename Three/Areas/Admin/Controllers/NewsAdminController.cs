﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Areas.Admin.ViewModels.News;
using Three.Models;
using Three.Models.Cmind;
using Three.Repositories;
using Three.Utility.Cmind;

namespace Three.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class NewsAdminController : BaseAdminController
    {
        private NewsRepository newsRepository = new NewsRepository();

        public ActionResult Index(NewsIndexView model)
        {
            var query = newsRepository.Query(model.Status, model.Title);
            model.DataList = Mapper.Map<IEnumerable<NewsIndexItemView>>(query);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new NewsView();
            model.OnlineTime = DateTime.Now;
            if (id != 0)
            {
                var query = newsRepository.FindBy(id);
                model = Mapper.Map<NewsView>(query);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(NewsView model, HttpPostedFileBase file)
        {
            var hasFile = ImageHelper.CheckFileExists(file);

            if (ModelState.IsValid)
            {
                News news = Mapper.Map<News>(model);

                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, news.MainImage);
                    news.MainImage = ImageHelper.SaveImage(file, PhotoFolder);
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                news.Updater = adminId;

                if (model.ID == 0)
                {
                    news.Creater = adminId;
                    model.ID = newsRepository.Insert(news);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    newsRepository.Update(news);
                    ShowMessage(true, "修改成功");
                }
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            DeleteImage(id);
            newsRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id)
        {
            string image = newsRepository.DeleteImage(id);
            ImageHelper.DeletePhoto(PhotoFolder, image);
            ShowMessage(true, "刪除圖片成功");
            return RedirectToAction("Edit", new { id = id });
        }

        /// <summary>
        /// html編輯器圖片上傳
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string result = "";
            if (upload != null && upload.ContentLength > 0)
            {
                //儲存圖片至Server
                upload.SaveAs(Server.MapPath("~/FileUploads/ImagePhoto/" + upload.FileName));


                var imageUrl = Url.Content("~/FileUploads/ImagePhoto/" + upload.FileName);

                var vMessage = string.Empty;

                result = @"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imageUrl + "\", \"" + vMessage + "\");</script></body></html>";

            }
            return Content(result);
        }
    }
}