﻿using Three.Areas.Admin.ViewModels.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.Repositories;
using Three.Models.Cmind;
using Three.Utility.Cmind;
using System.Web.Security;
using Three.ActionFilters;

namespace Three.Areas.Admin.Controllers
{
    [ErrorHandleAdminActionFilter]
    public class AuthAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository();

        // GET: Admin/AccountAdmin
        public ActionResult Login()
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                var controller = DependencyResolver.Current.GetService<AuthAdminController>();
                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
                return controller.Login(new LoginView() { Account = "sysadmin", Password = "123456" });
            }
#endif
            var login = new LoginView();
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var admin = adminRepository.Login(login.Account, login.Password);
                if (admin != null)
                {
                    AdminInfo adminInfo = new AdminInfo();
                    adminInfo.ID = admin.ID;
                    adminInfo.Account = admin.Account;
                    AdminInfoHelper.Login(adminInfo, login.RememberMe);
                    return Redirect(FormsAuthentication.GetRedirectUrl(login.Account, false));
                }
            }
            ShowMessage(false, "帳號或密碼錯誤");
            return View(login);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AdminInfoHelper.Logout();
            return RedirectToAction("Login");
        }
    }
}