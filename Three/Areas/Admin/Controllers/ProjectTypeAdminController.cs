﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Areas.Admin.ViewModels.ProjectType;
using Three.Models;
using Three.Repositories;
using Three.Utility.Cmind;

namespace Three.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProjectTypeAdminController : BaseAdminController
    {
        private ProjectTypeRepository projectTypeRepository = new ProjectTypeRepository();

        public ActionResult Index(ProjectTypeIndexView model)
        {
            var query = projectTypeRepository.Query(model.Status, model.Title);
            model.DataList = Mapper.Map<IEnumerable<ProjectTypeIndexItemView>>(query);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ProjectTypeView();
            if (id != 0)
            {
                var query = projectTypeRepository.FindBy(id);
                model = Mapper.Map<ProjectTypeView>(query);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectTypeView model, HttpPostedFileBase file)
        {
            var hasFile = ImageHelper.CheckFileExists(file);

            if (ModelState.IsValid)
            {
                ProjectType projectType = Mapper.Map<ProjectType>(model);

                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, projectType.Image);
                    projectType.Image = ImageHelper.SaveImage(file, PhotoFolder);
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                projectType.Updater = adminId;

                //if (model.ID == 0)
                //{
                //    projectType.Creater = adminId;
                //    model.ID = projectTypeRepository.Insert(projectType);
                //    ShowMessage(true, "新增成功");
                //}
                //else
                //{
                    projectTypeRepository.Update(projectType);
                    ShowMessage(true, "修改成功");
                //}
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    DeleteImage(id);
        //    projectTypeRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction("Index");
        //}

        public ActionResult DeleteImage(int id)
        {
            string image = projectTypeRepository.DeleteImage(id);
            ImageHelper.DeletePhoto(PhotoFolder, image);
            ShowMessage(true, "刪除圖片成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}