﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Areas.Admin.ViewModels.News;
using Three.Models;

namespace Three.Repositories
{
    public class NewsRepository
    {
        private ThreeDBEntities db = new ThreeDBEntities();

        #region 後台        
        public int Insert(News newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            newData.UpdateTime = now;
            db.News.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public void Update(News news)
        {
            DateTime now = DateTime.Now;
            News oldNews = db.News.Find(news.ID);
            oldNews.MainImage = news.MainImage;
            oldNews.Title = news.Title;
            oldNews.SubTitle = news.SubTitle;
            oldNews.Content = news.Content;
            oldNews.Status = news.Status;
            oldNews.OnlineTime = news.OnlineTime;
            oldNews.UpdateTime = now;            
            db.SaveChanges();
        }

        public IQueryable<News> GetAll()
        {
            var query = db.News;
            return query;
        }

        public IQueryable<News> Query(string status, string title)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(status))
            {
                bool b = status == "1";
                query = query.Where(p => p.Status == b);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title) || p.SubTitle.Contains(title));
            }
            return query;
        }

        public News FindBy(int id)
        {
            return db.News.Find(id);
        }

        public string DeleteImage(int id)
        {
            News news = db.News.Find(id);
            var image = news.MainImage;
            news.MainImage = string.Empty;
            db.SaveChanges();

            return image;
        }

        public void Delete(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);

            db.SaveChanges();
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public News Detail(int newsID)
        {
            var list = this.List();
            var query = list.Single(p => p.ID == newsID);

            return query;
        }

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<News> List()
        {
            var query = GetAll().Where(p => p.Status ==true && p.OnlineTime <= DateTime.Now);
            return query.OrderByDescending(p => p.OnlineTime);
        }
        #endregion
    }
}