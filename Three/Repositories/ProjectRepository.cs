﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models;

namespace Three.Repositories
{
    public class ProjectRepository
    {
        private ThreeDBEntities db = new ThreeDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        public int Insert(Project project)
        {
            DateTime now = DateTime.Now;
            project.CreateTime = now;
            project.UpdateTime = now;
            db.Project.Add(project);
            db.SaveChanges();
            return project.ID;
        }

        public void Update(Project project)
        {
            DateTime now = DateTime.Now;
            Project oldProject = db.Project.Find(project.ID);
            oldProject.MainImage = project.MainImage;
            oldProject.Title = project.Title;
            oldProject.SubTitle = project.SubTitle;
            oldProject.Content = project.Content;
            oldProject.Type = project.Type;
            oldProject.Link = project.Link;
            oldProject.AreaID = project.AreaID;
            oldProject.Address = project.Address;
            oldProject.Footage = project.Footage;
            oldProject.Status = project.Status;
            oldProject.Sort = project.Sort;
            oldProject.UpdateTime = now;
            db.SaveChanges();
        }

        public IQueryable<Project> GetAll()
        {
            var query = db.Project;
            return query;
        }

        public IQueryable<Project> Query(string status, string title, int areaId, int type = 0)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(status))
            {
                bool b = status == "1";
                query = query.Where(p => p.Status == b);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            if (type != 0)
            {
                query = query.Where(p => p.Type == type);
            }
            if (areaId != 0)
            {
                query = query.Where(p => p.AreaID == areaId);
            }
            
            return query;
        }

        public Project FindBy(int id)
        {
            Project project = db.Project.Find(id);            

            return project;
        }

        public IQueryable<Area> GetAllAreas()
        {
            var query = db.Area;
            return query;
        }

        public string DeleteImage(int id)
        {
            Project project = db.Project.Find(id);
            var image = project.MainImage;
            project.MainImage = string.Empty;
            db.SaveChanges();

            return image;
        }

        public IEnumerable<string> DeleteImageList(int id)
        {
            var images = db.Banner.Where(p => p.ProjectID == id).ToList();
            var result = images.Select(p => p.FileName);
            db.Banner.RemoveRange(images);
            db.SaveChanges();
            return result;
        }

        public void Delete(int id)
        {
            Project project = db.Project.Find(id);
            db.Project.Remove(project);

            db.SaveChanges();
        }
        #endregion

        #region 前台

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<Project> List()
        {
            return Query("1" , "", 0, 0);
        }

        public IQueryable<Project> SearchQuery(string searchValue)
        {
            var query = Query("1", "", 0);
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(p => p.Title.Contains(searchValue) || p.SubTitle.Contains(searchValue));
            }

            return query;
        }
        #endregion
    }
}