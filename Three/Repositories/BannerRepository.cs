﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models;

namespace Three.Repositories
{
    public class BannerRepository
    {
        private ThreeDBEntities db = new ThreeDBEntities();

        public IQueryable<Banner> GetImagesById(int projectId)
        {
            var query = db.Banner.Where(p => p.ProjectID == projectId).OrderBy(p => p.Sort);
            return query;
        }

        public IQueryable<Banner> Query(string status, string title, string type, int projectId)
        {
            IQueryable<Banner> query = db.Banner.Where(p => p.ProjectID == projectId);
            if (!string.IsNullOrEmpty(status))
            {
                bool b = status == "1";
                query = query.Where(p => p.Status == b);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            if (!string.IsNullOrEmpty(type))
            {
                int i = Convert.ToInt32(type);
                query = query.Where(p => p.Type == i);
            }
            return query;
        }

        public int Insert(Banner newData)
        {

            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            newData.UpdateTime = now;
            db.Banner.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public void Update(Banner data)
        {
            DateTime now = DateTime.Now;
            Banner update = db.Banner.Find(data.ID);
            update.Title = data.Title;
            update.FileName = data.FileName;
            update.Description = data.Description;
            update.ProjectID = data.ProjectID;
            update.Sort = data.Sort;
            update.Status = data.Status;
            update.UpdateTime = now;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Banner delete = db.Banner.Find(id);
            db.Banner.Remove(delete);
            db.SaveChanges();
        }

        public Banner FindBy(int id)
        {
            return db.Banner.Find(id);
        }


        public string DeleteImage(int id)
        {
            Banner image = db.Banner.Find(id);
            return image.FileName;
        }

        #region

        public IQueryable<Banner> List(int projectId,int type)
        {
            var query = db.Banner.Where(p => p.Status == true && p.ProjectID == projectId && p.Type == type).OrderBy(p => p.Sort);
            return query;
        }

        #endregion
    }
}