﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models;

namespace Three.Repositories
{
    public class AdminRepository
    {
        private ThreeDBEntities db = new ThreeDBEntities();

        public Admin Login(string account, string password)
        {
            var admin = db.Admin.Where(p => p.Account == account && p.Password == password && p.Status).FirstOrDefault();
            return admin;
        }
    }
}