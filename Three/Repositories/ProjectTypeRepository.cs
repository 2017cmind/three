﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Three.Models;

namespace Three.Repositories
{
    public class ProjectTypeRepository
    {
        private ThreeDBEntities db = new ThreeDBEntities();

        #region 後台        
        //public int Insert(ProjectType newData)
        //{
        //    DateTime now = DateTime.Now;
        //    newData.CreateTime = now;
        //    newData.UpdateTime = now;
        //    db.ProjectType.Add(newData);
        //    db.SaveChanges();
        //    return newData.ID;
        //}

        public void Update(ProjectType projectType)
        {
            DateTime now = DateTime.Now;
            ProjectType oldData = db.ProjectType.Find(projectType.ID);
            oldData.Image = projectType.Image;
            oldData.Title = projectType.Title;
            oldData.Status = projectType.Status;
            oldData.Sort = projectType.Sort;
            oldData.UpdateTime = now;
            db.SaveChanges();
        }

        public IQueryable<ProjectType> GetAll()
        {
            var query = db.ProjectType;
            return query;
        }

        public IQueryable<ProjectType> Query(string status, string title)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(status))
            {
                bool b = status == "1";
                query = query.Where(p => p.Status == b);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            return query;
        }

        public ProjectType FindBy(int id)
        {
            return db.ProjectType.Find(id);
        }

        public string DeleteImage(int id)
        {
            ProjectType projectType = db.ProjectType.Find(id);
            var image = projectType.Image;
            projectType.Image = string.Empty;
            db.SaveChanges();

            return image;
        }

        //public void Delete(int id)
        //{
        //    ProjectType projectType = db.ProjectType.Find(id);
        //    db.ProjectType.Remove(projectType);

        //    db.SaveChanges();
        //}


        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<ProjectType> List()
        {
            var query = GetAll().Where(p => p.Status == true);
            return query.OrderBy(p => p.Sort);
        }
        #endregion
    }
}