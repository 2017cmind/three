﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Three.Utility
{
    public class EnumClass
    {
    }

    /// <summary>
    /// 月份英文縮寫
    /// </summary>
    public enum EnglishShortMonth
    {
        Jan = 1, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    }

    ///// <summary>
    ///// 類別
    ///// </summary>
    //public enum PageType
    //{
    //    /// <summary>
    //    /// 一樓
    //    /// </summary>
    //    [Description("一樓")]
    //    firstFloor = 1,

    //    /// <summary>
    //    /// 旅館
    //    /// </summary>
    //    [Description("旅館")]
    //    hotelIntro = 2,

    //    /// <summary>
    //    /// 街區
    //    /// </summary>
    //    [Description("街區")]
    //    trafficInfo = 3
    //}
}