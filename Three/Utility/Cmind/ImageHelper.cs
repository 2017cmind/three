﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Three.Models.Cmind;

namespace Three.Utility.Cmind
{
    public class ImageHelper
    {
        private static readonly int imageSizeLimit = 3 ;

        /// <summary>
        /// 允許上傳圖片的副檔名
        /// </summary>
        private const string imageExtensionLimit = ".jpg.png";

        /// <summary>
        /// 儲存圖片
        /// </summary>
        /// <param name="photoFile">
        /// 要儲存的檔案
        /// </param>
        /// <param name="savePath">
        /// 儲存的路徑
        /// </param>
        /// /// <param name="fileName">
        /// 檔名，若輸入空字串則為yyyyMMddhhmmssfff
        /// </param>
        /// <returns>
        /// 儲存結果，
        /// 成功時Message為檔案名稱，若失敗則會回傳錯誤訊息
        /// </returns>
        public static string SaveImage(HttpPostedFileBase photoFile, string savePath, string fileName = "")
        {
            string extension = Path.GetExtension(photoFile.FileName);
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = GetFileName();
            }
            string fileNameWithExtension = string.Format("{0}{1}", fileName, extension);
            string path = Path.Combine(savePath, fileNameWithExtension);
            photoFile.SaveAs(path);
            return fileNameWithExtension;
        }

        /// <summary>
        /// 檢查圖片是否符合上傳格式
        /// </summary>
        /// <param name="photoFile">
        /// 要檢查的圖片檔案
        /// </param>
        /// <param name="required">
        /// 是否必要上傳檔案
        /// </param>
        /// <returns>
        /// 若沒有通過驗證會回傳原因，否則則回空字串        
        /// </returns>
        public static ImageCheckResult ImageCheck(HttpPostedFileBase photoFile)
        {
            var result = new ImageCheckResult();
            result.HasFile = CheckFileExists(photoFile);
            if (result.HasFile)
            {
                result.ErrorMessage = CheckIfFileSpec(photoFile);
            }
            else
            {
                result.ErrorMessage = string.Empty;
            }
            return result;
        }

        /// <summary>
        /// 檢查圖片是否存在
        /// </summary>
        /// <param name="photoFile"></param>
        /// <returns></returns>
        public static bool CheckFileExists(HttpPostedFileBase photoFile)
        {
            if (photoFile == null)
            {
                return false;
            }
            if (photoFile.ContentLength == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 檢查圖片是否符合格式
        /// 1.副檔名
        /// 2.檔案大小
        /// </summary>
        /// <param name="photoFile"></param>
        /// <returns></returns>
        public static string CheckIfFileSpec(HttpPostedFileBase photoFile)
        {
            string extension = Path.GetExtension(photoFile.FileName);
            if (imageExtensionLimit.IndexOf(extension) == -1)
            {
                return "圖片檔案格式不符";
            }
            int limit = imageSizeLimit * 1024 * 1024;
            if (photoFile.ContentLength > limit)
            {
                string message = string.Format("圖片檔案大小不得超過{0}MB", imageSizeLimit);
                return message;
            }
            return string.Empty;
        }

        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="filename">檔名</param>
        /// <param name="path">路徑</param>
        public static void DeletePhoto(string path, string filename)
        {
            filename = filename ?? "";
            string ComBinePath = Path.Combine(path, filename);

            if (File.Exists(ComBinePath))
            {
                File.Delete(ComBinePath);
            }
        }

        public static string GetFileName()
        {
            return DateTime.Now.ToString("yyyyMMddhhmmssfff");
        }
    }
}