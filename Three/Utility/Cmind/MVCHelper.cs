﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Three.Models;

namespace Three.Utility.Cmind
{
    public static class MVCHelper
    {
        /// <summary>
        /// 加入全部的選項 new SelectListItem { Text = "全部", Value = "" }
        /// </summary>
        /// <param name="selectListItems"></param>
        /// <returns></returns>
        public static List<SelectListItem> InsertAllOption(this List<SelectListItem> selectListItems)
        {
            selectListItems.Insert(0, new SelectListItem { Text = "全部", Value = "" });
            return selectListItems;
        }

        /// <summary>
        /// 26 jan 18
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string DatetimeFormat(DateTime dt)
        {
            return dt.ToString("yyyy.MM.dd");
        }

        public static string GetEnglishShortMonth(int month)
        {
            EnglishShortMonth englishMonth = (EnglishShortMonth)month;
            return englishMonth.ToString();
        }

        public static string StripHTML(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            return Regex.Replace(input, "<.*?>", String.Empty);
        }
    }
}