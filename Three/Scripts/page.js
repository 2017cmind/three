﻿//分頁
$(function () {
    var urlVars = getUrlVars();

    $('.page_pagination li:not(.disabled)').on('click', function () {
        const pager = $(this).attr('data-pager');
        urlVars['CurrentPage'] = pager;
        goToPage(urlVars);
    });  
})