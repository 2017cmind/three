﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Models;
using Three.Models.Cmind;
using Three.Repositories;
using Three.Utility.Cmind;
using Three.ViewModels.Banner;
using Three.ViewModels.Project;
using static Three.Models.EnumClass;

namespace Three.Controllers
{
    [ErrorHandleActionFilter]
    public class ProjectController : Controller
    {
        private ProjectRepository projectRepository = new ProjectRepository();
        private ProjectTypeRepository projectTypeRepository = new ProjectTypeRepository();
        private BannerRepository bannerRepository = new BannerRepository();

        // GET: Project
        /// <summary>
        /// 案例主頁
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public ActionResult Index(int areaId = 1)
        {
            ProjectIndexView model = new ProjectIndexView();
            var query = projectRepository.Query("1", "", 0, 0).ToList();
            List<ProjectView> projectList = Mapper.Map<List<ProjectView>>(query).ToList();
            model.ProjectAreaList = projectList.Where(p => p.AreaID == areaId).ToList();
            model.AreaList = GetAreaListItem();
            var typeQuery = projectTypeRepository.List();
            model.TypeList = Mapper.Map<List<ProjectTypeListView>>(typeQuery).ToList();

            return View(model);
        }

        /// <summary>
        /// 案例主頁區域切換
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(ProjectIndexView model)
        {
            var query = projectRepository.Query("1", "", 0, 0).ToList();
            List<ProjectView> projectList = Mapper.Map<List<ProjectView>>(query).ToList();
            model.ProjectAreaList = projectList.Where(p => p.AreaID == model.SearchArea).ToList();
            model.AreaList = GetAreaListItem();
            var typeQuery = projectTypeRepository.List();
            model.TypeList = Mapper.Map<List<ProjectTypeListView>>(typeQuery).ToList();
            return View(model);
        }

        /// <summary>
        /// 案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult ProjectType(ProjectTypeListView model, int type, string searchValue = "")
        {
            var query = projectRepository.Query("1", "", 0, 0);
            model.ProjectList = Mapper.Map<List<ProjectView>>(query).ToList();

            string projectTypeTitle = string.Empty;

            var typeQuery = projectTypeRepository.List();
            model.ProjectTypeList = Mapper.Map<List<ProjectView>>(typeQuery).ToList();

            model.PageSize = 6;
            if (type != 0)
            {
                projectTypeTitle = projectTypeRepository.FindBy(type).Title;
                query = projectRepository.Query("1", "", 0, type);
            }

            if (!string.IsNullOrEmpty(searchValue))
            {
                query = projectRepository.SearchQuery(searchValue);
            }
            var pageResult = query.ToPageResult<Project>(model);
            model.PageResult = Mapper.Map<PageResult<ProjectView>>(pageResult);

            model.Title = String.IsNullOrEmpty(projectTypeTitle) ? "案例搜尋" : projectTypeTitle;

            return View(model);
        }

        /// <summary>
        /// 案例內容頁
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(ProjectView model, int id)
        {
            var query = projectRepository.FindBy(id);
            model = Mapper.Map<ProjectView>(query);

            model.ProjectTypeTitle = projectTypeRepository.FindBy(model.Type).Title;
            model.Area = projectRepository.GetAllAreas().Where(a => a.ID == model.AreaID).FirstOrDefault().Title;

            var typeQuery = projectTypeRepository.List();
            model.ProjectTypeList = Mapper.Map<IEnumerable<ProjectView>>(typeQuery).ToList();

            var ProjectListTypeQuery = projectRepository.Query("1", "", 0, 0);
            model.ProjectList = Mapper.Map<List<ProjectView>>(ProjectListTypeQuery).ToList();

            model.ImageList = Mapper.Map<List<BannerView>>(bannerRepository.List(id, 0));
            return View(model);
        }

        /// <summary>
        /// 案例圖覽頁
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detailphoto(ProjectPhotoView model, int id)
        {
            var query = projectRepository.FindBy(id);
            model = Mapper.Map<ProjectPhotoView>(query);
            model.ProjectTypeTitle = projectTypeRepository.FindBy(model.Type).Title;

            var typeQuery = projectTypeRepository.List();
            model.ProjectTypeList = Mapper.Map<IEnumerable<ProjectView>>(typeQuery).ToList();

            var ProjectListTypeQuery = projectRepository.Query("1", "", 0, 0);
            model.ProjectList = Mapper.Map<List<ProjectView>>(ProjectListTypeQuery).ToList();

            model.ImageList = Mapper.Map<List<BannerView>>(bannerRepository.List(id,0));

            return View(model);
        }

        /// <summary>
        /// 搜尋頁
        /// </summary>
        /// <returns></returns>
        public ActionResult Search()
        {
            SearchView model= new SearchView();

            return View(model);
        }

        /// <summary>
        /// 搜尋
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search(SearchView model)
        {
            if (string.IsNullOrEmpty(model.SearchValue))
            {
                return View(model);
            }
            return RedirectToAction("ProjectType", "Project", new { type = 0, searchValue = model.SearchValue});
        }

        /// <summary>
        /// 案例區域清單
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetAreaListItem()
        {
            List<SelectListItem> areaList = new List<SelectListItem>();
            List<Area> AreaList = projectRepository.GetAllAreas().Where(a => a.Status == true).ToList();
            for (int i = 0; i < AreaList.Count; i++)
            {
                areaList.Add(new SelectListItem()
                {
                    Text = AreaList[i].Title,
                    Value = (AreaList[i].ID).ToString(),
                    Selected = false
                });
            }

            return areaList;
        }


        [HttpPost]
        public JsonResult GetProjectAreaList(int areaId)
        {
            List<Project> projectList = projectRepository.Query("1", "", 0, 0).Where(p => p.AreaID == areaId && p.Type != 2).ToList();

            JsonResult result = Json(projectList);

            return result;
        }
    }
}