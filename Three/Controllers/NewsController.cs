﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Models;
using Three.Models.Cmind;
using Three.Repositories;
using Three.Utility.Cmind;
using Three.ViewModels.News;

namespace Three.Controllers
{
    [ErrorHandleActionFilter]
    public class NewsController : Controller
    {
        private NewsRepository newsRepository = new NewsRepository();

        public ActionResult Index(NewsIndexView model)
        {
            var query = newsRepository.List();
            model.PageSize = 6;
            var pageResult = query.ToPageResult<News>(model);
            model.PageResult = Mapper.Map<PageResult<NewsView>>(pageResult);
            return View(model);
        }

        public ActionResult Detail(int id = 0)
        {
            NewsView model = new NewsView();
            if (id != 0)
            {
                var query = newsRepository.FindBy(id);
                model = Mapper.Map<NewsView>(query);
            }
            return View(model);
        }
        /// <summary>
        /// 相關新聞
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult _News()
        {
            var query = newsRepository.List().Take(4);
            var model = Mapper.Map<IEnumerable<NewsView>>(query);
            return PartialView(model);
        }
    }
}