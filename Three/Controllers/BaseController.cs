﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Three.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// 錯誤頁面
        /// </summary>
        /// <param name="errorMessage">
        /// 錯誤訊息
        /// </param>
        /// <returns></returns>
        public ActionResult Error(string errorMessage = "")
        {
            TempData["ErrorMessage"] = errorMessage;
            return View("~/Views/Shared/Error.cshtml");
        }
    }
}