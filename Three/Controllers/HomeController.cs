﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Three.ActionFilters;
using Three.Repositories;
using Three.ViewModels.Banner;
using Three.ViewModels.Home;
using Three.ViewModels.News;
using Three.ViewModels.Project;

namespace Three.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : Controller
    {
        private NewsRepository newsRepository = new NewsRepository();
        private BannerRepository bannerRepository = new BannerRepository();
        public ActionResult Index()
        {
            HomeView model = new HomeView();

            var queryNews = newsRepository.List().Take(4);
            model.NewsList = Mapper.Map<IEnumerable<NewsView>>(queryNews);

            var query_PCBanner = bannerRepository.List(0, 1);
            model.PCBannerList = Mapper.Map<IEnumerable<BannerView>>(query_PCBanner);

            var query_MoblieBanner = bannerRepository.List(0, 2);
            model.MoblieBannerList = Mapper.Map<IEnumerable<BannerView>>(query_MoblieBanner);

            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }

        public ActionResult Service()
        {

            return View();
        }
    }
}